import xadmin
from .models import Category
from .models import Post
from .models import Category

class CategoryAdmin(object):
    list_display = ('name',)
    list_filter = ('name',)
    search_fields = ('name',)

class PostAdmin(object):
    list_display = ('title', 'post_num', 'body', 'category', 'author')
    list_filter = ('title', 'post_num', 'body', 'category', 'author')
    search_fields = ('title', 'post_num', 'body', 'category', 'author')






xadmin.site.register(Post, PostAdmin)

xadmin.site.register(Category, CategoryAdmin)

import xadmin

