from django.db import models
# from django.contrib.auth.models import User
# Create your models here.
from users.models import UsersProfile
from django.urls import reverse
# 分类


class Category(models.Model):
    name = models.CharField(max_length=100)

# 文章


class Post(models.Model):
    title = models.CharField('标题', max_length=100)
    post_num = models.IntegerField('热度',default=0)
    body = models.TextField()
    category = models.ForeignKey(
        Category, default='2', on_delete=models.CASCADE)
    author = models.ForeignKey(UsersProfile, related_name="users",
                               verbose_name="作者 ID", default="1", on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'pk': self.pk})
