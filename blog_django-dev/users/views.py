from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.views import View
from users.users_forms import UserRegister, UserRegisterForm
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import View
from .models import UsersProfile
from django.views.generic import ListView, DetailView
from mydata.models import Post
from .froms import ContactForm
# post_index


def post_data(request):
    post = Post.objects.all().order_by("-post_num")[:10]
    return render(request, 'mydata/post_index.html', context={"post_list": post})

# Create your views here.
# login_url = reverse_lazy("users:usersLogin")

# 没有实现


def add(request):
    title = request.POST.get('title', None)
    body = request.POST.get('content', None)
    # print(body)
    author = request.user
    article = Post(title=title, body=body, author=author)
    article.save()
    return HttpResponse("成功")


# def add_post(request):
#     return render(request, 'mydata/mydata.html')
#     # if request.method == 'GET':
#     #     return render(request,'mydata/mydata.html')
#     # if request.method == 'POST':
#     #     title=request.Post.gey("title",None)
#     #     body=request.Post.gey("body",None)
#     #     post=Post.objects.create(title=title,body=body)
#     #     post.save()


class UsersLogin(LoginView):
    template_name = "users/login.html"


class UsersLogout(LogoutView):
    next_page = reverse_lazy('index')


class UserRegisterFormView(FormView):
    template_name = 'users/register.html'
    form_class = UserRegisterForm
    success_url = reverse_lazy('users:usersLogin')

    def form_valid(self, form):
        from django.contrib.auth.hashers import make_password
        user = UsersProfile(**form.cleaned_data)
        user.set_password(form.cleaned_data['password'])
        user.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        # 数据无效返回原来的模板，并且携带原来提交的数据
        print("form-->", form)
        return super().form_invalid(form)


# 展示
class ServerNowListView(LoginRequiredMixin, ListView):
    login_url = '/login/'
    login_url = reverse_lazy("users:usersLogin")
    model = Post
    context_object_name = "serverList"
    template_name = "server-list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['now'] = timezone.now()
        return context
# 详情


def ServerDetail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post_num = post.post_num + 1
    post.post_num = post_num
    post.save()
    return render(request, 'mydata/server-detail.html', context={'post_list': post})

# class ServerDetail(LoginRequiredMixin, DetailView):
# class ServerDetail(LoginRequiredMixin, DetailView):
    # login_url = reverse_lazy("users:usersLogin")
    # queryset=Post.objects.filter()
    # queryset +=1
    # queryset.save()
    # model = Post
    # context_object_name = "post_list"
    # template_name = "mydata/server-detail.html"


class personal(View):
    def get(self, request):
        login_url = '/login/'
        art_count = Post.objects.filter(author=request.user).count()
        return render(request, 'users/user_personal.html', context={'art_count': art_count})


def toadd(request):
    return render(request, 'mydata/mydata.html')
