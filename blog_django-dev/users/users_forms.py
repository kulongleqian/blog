import re
from django import forms
from users.models import UsersProfile

gender_choice = (
    ('1', "男"),
    ('2', "女")
)


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = UsersProfile
        fields = ['username', 'password', 'mobile', 'age',"avatar"]

    def clean_mobile(self):
        """
        验证手机号是否合法
        :return: 合法的数据或者错误信息
        """
        mobile = self.cleaned_data['mobile']
        PRGEX_MOBILE = r'^1[358]\d{9}|^147\d{8}|^176\d{8}$'
        regex = re.compile(PRGEX_MOBILE)
        if regex.match(mobile):
            return mobile
        else:
            raise forms.ValidationError(
                '无效的手机号',
                code='mobile_invalid'
            )


class UserRegister(forms.Form):

    username = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    password = forms.CharField(
        required=True,   # 必填项
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}),  # textinput
    )
    mobile = forms.CharField(
        min_length=11,  # 设置手机号长度
        max_length=11,
        required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    age = forms.IntegerField(
        required=True,
        min_value=1,  # 设置最大最小值
        max_value=150,
        widget=forms.NumberInput(attrs={'class': 'form-control'}),
    )
    gender = forms.ChoiceField(required=True,
                               choices=gender_choice,
                               widget=forms.RadioSelect(),
                               initial=1,   # 默认选择
                               )
    # 设置输入数据的规则

    def clean_mobile(self):
        mobile = self.cleaned_data['mobile']
        PRGEX_MOBILE = r'^1[358]\d{9}|^147\d{8}|^176\d{8}$'
        regex = re.compile(PRGEX_MOBILE)
        if regex.match(mobile):
            return mobile
        else:
            raise forms.ValidationError(
                '无效的手机号',
                code='mobile_invalid'
            )
