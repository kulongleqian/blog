from django.contrib import admin
from django.urls import path, include
from .views import (
    UsersLogin, UsersLogout, UserRegisterFormView,
    ServerNowListView, ServerDetail, post_data, personal, add, toadd


)
app_name = "users"
urlpatterns = [
    path('login/', UsersLogin.as_view(), name="usersLogin"),
    path('logout/', UsersLogout.as_view(), name="usersLogout"),
    path('register/', UserRegisterFormView.as_view(), name="Register"),
    path("show/", ServerNowListView.as_view(), name="serverNowList"),
    path("post/<int:pk>/", ServerDetail, name="serverdetail"),
    path("post_data/", post_data, name="post_data"),
    path(r'^Add/$', add, name="add_data"),
    path(r'^toadd/$', toadd, name="toadd"),
    path("personal/", personal.as_view(), name="personal"),

]
